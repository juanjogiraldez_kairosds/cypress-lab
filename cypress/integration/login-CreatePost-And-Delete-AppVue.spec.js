/// <reference types="Cypress" />

it('simple', () => {
    cy.visit('http://localhost:8080/Homemain');
    cy.get(':nth-child(2) > .hydrated > div > a > button').click();
    cy.get('button').click();
    cy.get(':nth-child(2) > a').click();
    cy.get('.user').type('user1');
    cy.get('.pass').type('pass');
    cy.get('.login').click();
    cy.get('.p-clearfix > .p-button').click();
    cy.get('#author').type('user1');
    cy.get('#nickName').type('Juanichi');
    cy.get('#title').type('Presentación Project-Front-Vue');
    cy.get('#text').type('Esto es el proceso de los test con Cypress, WoaooooHHH!!!');
    cy.get('.p-button-info').click();
    cy.get(':nth-child(12) > [style="text-align: center;"] > .p-button-danger').click();
})